$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
      interval: 5500
    });
    
    $('#newsletter').on('show.bs.modal', function (e) {
      console.log('El modal se esta abriendo.');
      $('#newsletterBtn').removeClass('Btn-newsletter');
      $('#newsletterBtn').addClass('btn-light');
      $('#newsletterBtn').prop('disabled', true);

    });
    $('#newsletter').on('shown.bs.modal', function (e) {
      console.log('El modal se abrió.');
    });
    $('#newsletter').on('hide.bs.modal', function (e) {
      console.log('El modal se esta ocultando.');
    });
    $('#newsletter').on('hidden.bs.modal', function (e) {
      console.log('El modal se ocultó.');
      $('#newsletterBtn').prop('disabled', false);
      $('#newsletterBtn').removeClass('btn-light');
      $('#newsletterBtn').addClass('Btn-newsletter'); 
    });
  });